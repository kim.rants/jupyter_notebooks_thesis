# Data exploration for understanding glioblastoma through radiogenomics

This Gitlab account includes the Jupyter Notebooks used for producing the above project:
"Data exploration for understanding glioblastoma through radiogenomics", Imperial College London 2018


## Overview

There are two main folders: i) Pipelines, and ii) Feature_extraction


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


### Prerequisites

What things you need to install the software and how to install them

```
- We recommend using Anaconda
- PyRadiomics required for feature extraciton
```

```



## Authors

* **Kim Rants** 
* **Elsa Angelini** 



## Acknowledgments

Elsa Angelini
Paul Blakeley
Jazz Smith
Jeremy Cohen
